#!/bin/ksh
#
# hd_subdaily_settings.ksh - Sub-script with settings for sub-daily HD runs sourced from hd_run_settings.ksh
# 
# Copyright (C) 2025, Institute of Coastal Systems - Analysis and Modelling, Helmholtz-Zentrum Hereon
# SPDX-License-Identifier: Apache-2.0
# See ./LICENSES/ for license information
#
# Authors: Stefan Hagemann
# Contact: <stefan.hagemann@hereon.de>
#_________________________________________
#
# HD sub-daily settings
echo " ***** HD subdaily settings *****" 
#
# Start and end dates/times (Note that time 24:00:00 is not allowed)
date_start=${YYYY}0707   ;   date_end=${YYYY}0707
time_start="00:00:00"  ;  time_end="04:00:00"       # time_end is starting time of final time step
xd="$date_start"
MM=${xd:4:2}  ;  YYYY=${xd:0:4}
echo " Startdate: $date_start time: $time_start  Enddate: $date_end  time: $time_end" 
#
# set time step: no. of time steps per day
ndt_set=48

if (( $ndt_set > 0 )) ; then
  echo " Number of time steps per day set to $ndt_set" 
fi
#
# Write an additional retart file
date_rest=${YYYY}0707
time_rest="03:00:00"
#
